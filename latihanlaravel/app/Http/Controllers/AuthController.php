<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar(){
        return view('datainput');
    }
    
    public function akhirr(Request $request){
        $ndepan = $request['depan'];
        $nbelakang = $request['belakang'];
        return view('akhir', ['ndepan' => $ndepan, 'nbelakang' => $nbelakang]);
    }
}
