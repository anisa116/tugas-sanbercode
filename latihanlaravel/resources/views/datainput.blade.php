<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up</h2>
    <form action="/kirim" method="post">
        @csrf
            <label>First name</label> <br>
            <input type="text" name="depan"><br><br>

            <label>Last Name</label><br>
            <input type="text" name="belakang"><br><br>

            <label>Gender</label><br>
            <input type="radio" name="jenis_kelamin" value="L"> Laki-laki <br>
            <input type="radio" name="jenis_kelamin" value="P"> Perempuan<br> <br>

            <label>Nationally</label><br>
            <select name="jk">
                <option value="ind">Indonesia</option>
                <option value="eng">English</option>
                <option value="arab">Arabic</option>
                <option value="jap">Japanese</option>
            </select><br><br>

            <label>Language Spoken</label><br>
                <input type="checkbox" name="ind" value="ind"> Indonesia
                <br>
                <input type="checkbox" name="eng" value="eng"> English
                <br>
                <input type="checkbox" name="arab" value="arab"> Arabic
                <br>
                <input type="checkbox" name="jap" value="japan"> Japanese
                <br><br>
    
            <label>Bio</label><br>
                <textarea name="" cols="30" rows="5" align="center"></textarea>
                <br><br>
            
                <input type="submit" value="kirim">
                
    </form>
</body>
</html>